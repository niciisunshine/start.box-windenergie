import time

from machine import ADC

import constants
from webserver import WebServer
from wlan import WlanAP as Wlan


class RuntimeVariables:
    _wlan = Wlan()
    _webserver = WebServer("0.0.0.0", 80)
    _passed_time = constants.START_TICKS
    _pulses_to_count = 15
    _counter = _pulses_to_count
    _ready_to_calculate = False
    _current_rpm = 0
    _generator_voltage_adc = ADC(constants.GENERATOR_VOLTAGE_PIN)
    _operating_voltage_adc = ADC(constants.OPERATING_VOLTAGE_PIN)
    _current_generator_voltage = 0
    _current_operating_voltage = 0

    @classmethod
    def init(cls) -> None:
        cls._generator_voltage_adc.atten(ADC.ATTN_11DB)
        cls._operating_voltage_adc.atten(ADC.ATTN_11DB)

    @classmethod
    def update_on_hall_pulse(cls) -> None:
        round_trip_time = time.ticks_diff(time.ticks_ms(), cls._passed_time)
        cls._passed_time = time.ticks_ms()

        cls._current_rpm = 4_000 // round_trip_time

        cls._counter -= 1
        if cls._counter == 0:
            cls._counter = cls._pulses_to_count
            cls._ready_to_calculate = True

    @classmethod
    def check_current_rpm(cls) -> None:
        round_trip_time = time.ticks_diff(time.ticks_ms(), cls._passed_time)

        if round_trip_time > 4_000:
            cls._current_rpm = 0

    @classmethod
    def is_ready_to_calculate_and_reset(cls) -> bool:
        is_ready = cls._ready_to_calculate
        cls._ready_to_calculate = False
        return is_ready

    @classmethod
    def update_voltage(cls) -> None:
        generator_voltage = cls._generator_voltage_adc.read_uv()
        time.sleep_ms(10)
        generator_voltage += cls._generator_voltage_adc.read_uv()
        time.sleep_ms(10)
        generator_voltage += cls._generator_voltage_adc.read_uv()

        cls._current_generator_voltage = generator_voltage / 3 / 1_000_000

        operating_voltage = cls._operating_voltage_adc.read_uv()
        time.sleep_ms(10)
        operating_voltage += cls._operating_voltage_adc.read_uv()
        time.sleep_ms(10)
        operating_voltage += cls._operating_voltage_adc.read_uv()

        cls._current_operating_voltage = operating_voltage / 3 / 1_000_000 * 2.92

    @classmethod
    def get_current_rpm(cls) -> int:
        return cls._current_rpm

    @classmethod
    def get_generator_voltage(cls) -> int:
        return cls._current_generator_voltage

    @classmethod
    def get_operating_voltage(cls) -> int:
        return cls._current_operating_voltage

    @classmethod
    def get_wlan(cls) -> Wlan:
        return cls._wlan

    @classmethod
    def get_webserver(cls) -> WebServer:
        return cls._webserver
