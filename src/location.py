from http import BaseResponse, Request

from views import get_values, index_html, random_values, reset_values

try:
    from typing import Callable
except ImportError:
    Callable = None

LOCATIONS: dict[str, Callable[[Request], BaseResponse]] = {
    "/": index_html,
    "/reset": reset_values,
    "/values": get_values,
    # TODO remove random
    "/random": random_values,
}
