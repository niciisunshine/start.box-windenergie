import select
import socket
from http import Request, Response500

from location import LOCATIONS
from real_time import is_real_time_fetched, iso8601
from views import static_file


class SocketWrapper:
    def __init__(self, _socket: socket.Socket) -> None:
        self._socket = _socket

    def __enter__(self) -> socket.Socket:
        return self._socket

    def __exit__(self, *args) -> None:
        self._socket.close()


class WebServer:
    def __init__(self, address: str, port: int) -> None:
        self._address = address
        self._port = port
        self._server: socket.Socket | None = None
        self._poll: select.Poll | None = None

    def __enter__(self) -> "WebServer":
        self.boot()
        return self

    def __exit__(self, *args) -> None:
        self.close()

    def boot(self) -> None:
        self._server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._server.bind((self._address, self._port))
        self._server.listen()

        self._poll = select.poll()
        self._poll.register(self._server, select.POLLIN)

    def non_blocking_accept_and_respond(self) -> None:
        if self._can_accept():
            connection, (address, port) = self._server.accept()
            with SocketWrapper(connection):
                request = self._receive(connection)

                view = LOCATIONS.get(request.location, static_file)

                try:
                    response = view(request)
                except BaseException as exc:
                    print("Error while handling request:", repr(exc))
                    response = Response500()

                for chunk in response.get_chunks():
                    connection.sendall(chunk)

                print(
                    address,
                    "-",
                    *([f"[{iso8601()}]"] if is_real_time_fetched() else []),
                    f'"{str(request)}"',
                    response.status_code,
                    response.length(),
                    f'"{request.get_single_header("User-Agent", "")}"',
                    f'"{request.get_single_header("Referer", "")}"',
                )

    def close(self):
        self._server.close()

    def _can_accept(self) -> bool:
        events = self._poll.poll(10)

        for obj, event, *_ in events:
            if obj == self._server:
                if event & (select.POLLERR | select.POLLHUP):
                    raise ValueError("An unexpected error occurred while polling the socket")

                return bool(event & select.POLLIN)

        return False

    @staticmethod
    def _parse_head(head: bytes) -> tuple[str, str, str, list[tuple[str, str]]]:
        decoded_head = head.decode("ISO-8859-1").splitlines()
        method, location, http_version = decoded_head[0].split(" ")
        headers = list()
        for header in decoded_head[1:]:
            key, _, value = header.partition(": ")
            headers.append((key, value))
        return method, location, http_version, headers

    @classmethod
    def _receive(cls, connection: socket.Socket) -> Request:
        head = bytearray()
        body: bytearray

        while True:
            received: bytes = connection.recv(1024)

            if b"\r\n\r\n" in received:
                head_end, _, body_start = received.partition(b"\r\n\r\n")
                head += head_end
                body = bytearray(body_start)
                break
            else:
                head += received

        method, location, http_version, headers = cls._parse_head(head)
        try:
            content_length = int(list(value for key, value in headers if key == "Content-Length")[0])
        except IndexError:
            content_length = 0

        while len(body) < content_length:
            body += connection.recv(1024)

        return Request(method, location, http_version, headers, body)
