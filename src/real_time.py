import machine
import ntptime

_real_time_fetched = False


def iso8601():
    year, month, day, _, hours, minutes, seconds, __ = machine.RTC().datetime()
    return f"{year:04d}-{month:02d}-{day:02d}T{hours:02d}:{minutes:02d}:{seconds:02d}+00:00"


def fetch_real_time():
    global _real_time_fetched

    ntptime.host = "0.at.pool.ntp.org"
    ntptime.settime()
    _real_time_fetched = True


def is_real_time_fetched() -> bool:
    return _real_time_fetched
