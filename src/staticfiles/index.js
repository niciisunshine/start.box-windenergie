window.addEventListener("load", function() {
    const barContainerTooltips = [];
    for (let i = 0; i < 13; ++i) {
        const barContainer = document.getElementById(`rotations-${i}-bar-container`);
        barContainerTooltips.push(new bootstrap.Tooltip(barContainer, {animation: false, title: "0", container: document.body}));
    }

    function formatDuration(duration) {
        const seconds = duration % 60;
        const minutes = Math.floor(duration / 60) % 60;
        const hours = Math.floor(duration / 3_600) % 24;
        const days = Math.floor(duration / 86_400);

        const secondsText = seconds.toFixed() + " " + (seconds === 1 ? "Sekunde" : "Sekunden");
        const minutesText = minutes.toFixed() + " " + (minutes === 1 ? "Minute" : "Minuten");
        const hoursText = hours.toFixed() + " " +  (hours === 1 ? "Stunde" : "Stunden");
        const daysText = days.toFixed() + " " + (days === 1 ? "Tag" : "Tage");

        return `${daysText} ${hoursText} ${minutesText} ${secondsText}`;
    }

    async function updatePage() {
        const response = await fetch("/values");
        const data = await response.json();

        document.getElementById("uptime").innerText = formatDuration(data.uptime);

        const maxRotations = Math.max(...data.rotations);

        for (let i = 0; i < 13; ++i) {
            const percentage = maxRotations === 0 ? 0 : (data.rotations[i] / maxRotations);
            const barWidth = (percentage * 100).toFixed(2) + "%";
            barContainerTooltips[i].setContent({".tooltip-inner": data.rotations[i].toFixed()});
            document.getElementById(`rotations-${i}-bar`).style.width = barWidth;

            const barTitle = document.getElementById(`rotations-${i}-bar-title`);
            if (percentage < 0.4) {
                barTitle.style.left = barWidth;
                barTitle.classList.add("outside");
            } else {
                barTitle.style.left = "0";
                barTitle.classList.remove("outside");
            }
        }

        document.getElementById("current-rpm").innerText = data.currentRPM.toFixed();
        document.getElementById("current-generator-voltage").innerText = data.currentGeneratorVoltage.toFixed(2);
        document.getElementById("current-operating-voltage").innerText = data.currentOperatingVoltage.toFixed(2);
    }

    async function updatePageLoop() {
        while (true) {
            const startTime = Date.now();
            try {
                await updatePage();
            } catch (e) {
                console.error(e);
            }
            const timeElapsed = Date.now() - startTime;
            const timeToSleep = 1_000 - timeElapsed;

            if (timeToSleep > 0) {
                await new Promise(resolve => setTimeout(resolve, timeToSleep));
            }
        }
    }

    updatePageLoop();
});

async function sendReset() {
    const passwordInput = document.getElementById("reset-password");
    const passwordInvalidFeedback = document.getElementById("reset-password-invalid-feedback");
    const passwordUnexpectedError = document.getElementById("reset-unexpected-error");
    const passwordUnexpectedErrorCount = document.getElementById("reset-unexpected-error-count");

    function renderUnexpectedErrorMessage() {
        passwordInput.classList.remove("is-invalid");
        passwordUnexpectedError.classList.remove("d-none");

        const count = Number(passwordUnexpectedErrorCount.innerText) + 1;
        passwordUnexpectedErrorCount.innerText = count.toFixed();
        if (count > 1) {
            passwordUnexpectedErrorCount.classList.remove("d-none");
        }
    }

    try {
        const response = await fetch("/reset", {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({password: passwordInput.value})
        });

        if (response.status === 200) {
            bootstrap.Modal.getOrCreateInstance(document.getElementById("reset-modal")).hide();
        } else if (response.status === 403) {
            passwordInput.value = "";
            passwordInput.classList.add("is-invalid");
            passwordUnexpectedError.classList.add("d-none");
            passwordUnexpectedErrorCount.classList.add("d-none");
            passwordUnexpectedErrorCount.innerText = "0";
        } else {
            renderUnexpectedErrorMessage();
        }
    } catch (_) {
        renderUnexpectedErrorMessage();
    }
}

async function sendRandom() {
    const passwordInput = document.getElementById("random-password");
    const passwordInvalidFeedback = document.getElementById("random-password-invalid-feedback");
    const passwordUnexpectedError = document.getElementById("random-unexpected-error");
    const passwordUnexpectedErrorCount = document.getElementById("random-unexpected-error-count");

    function renderUnexpectedErrorMessage() {
        passwordInput.classList.remove("is-invalid");
        passwordUnexpectedError.classList.remove("d-none");

        const count = Number(passwordUnexpectedErrorCount.innerText) + 1;
        passwordUnexpectedErrorCount.innerText = count.toFixed();
        if (count > 1) {
            passwordUnexpectedErrorCount.classList.remove("d-none");
        }
    }

    try {
        const response = await fetch("/random", {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify({password: passwordInput.value})
        });

        if (response.status === 200) {
            bootstrap.Modal.getOrCreateInstance(document.getElementById("random-modal")).hide();
        } else if (response.status === 403) {
            passwordInput.value = "";
            passwordInput.classList.add("is-invalid");
            passwordUnexpectedError.classList.add("d-none");
            passwordUnexpectedErrorCount.classList.add("d-none");
            passwordUnexpectedErrorCount.innerText = "0";
        } else {
            renderUnexpectedErrorMessage();
        }
    } catch (_) {
        renderUnexpectedErrorMessage();
    }
}

window.addEventListener("load", function() {
    document.getElementById("reset-password").addEventListener("keyup", function(event) {
        if (event.key === "Enter") {
            sendReset();
        }
    });

    document.getElementById("random-password").addEventListener("keyup", function(event) {
        if (event.key === "Enter") {
            sendRandom();
        }
    });

    document.getElementById("reset-modal").addEventListener("shown.bs.modal", function(event) {
        document.getElementById("reset-password").focus();
    });

    document.getElementById("random-modal").addEventListener("shown.bs.modal", function(event) {
        document.getElementById("random-password").focus();
    });

    document.getElementById("reset-modal").addEventListener("hidden.bs.modal", function(event) {
        const passwordInput = document.getElementById("reset-password");
        passwordInput.value = "";
        passwordInput.classList.remove("is-invalid");

        document.getElementById("reset-unexpected-error").classList.add("d-none");

        const passwordUnexpectedErrorCount = document.getElementById("reset-unexpected-error-count");
        passwordUnexpectedErrorCount.classList.add("d-none");
        passwordUnexpectedErrorCount.innerText = "0";
    });

    document.getElementById("random-modal").addEventListener("hidden.bs.modal", function(event) {
        const passwordInput = document.getElementById("random-password");
        passwordInput.value = "";
        passwordInput.classList.remove("is-invalid");

        document.getElementById("random-unexpected-error").classList.add("d-none");

        const passwordUnexpectedErrorCount = document.getElementById("random-unexpected-error-count");
        passwordUnexpectedErrorCount.classList.add("d-none");
        passwordUnexpectedErrorCount.innerText = "0";
    });
});