import json

try:
    from typing import Any, Iterator
except ImportError:
    Any = None
    Iterator = None


class Request:
    def __init__(self, method: str, location: str, http_version: str, headers: list[tuple[str, str]], body: bytes):
        self.method = method
        self.location = location
        self.http_version = http_version
        self.headers = headers
        self.body = body

    def __str__(self):
        return f"{self.method} {self.location} {self.http_version}"

    def __getitem__(self, item: str) -> list[str]:
        return list(value for key, value in self.headers if key == item)

    def get_single_header(self, key: str, default: str | None = None) -> str:
        try:
            return self[key][0]
        except IndexError as exc:
            if default is not None:
                return default
            raise exc


class BaseResponse:
    def __init__(
        self,
        content_type: str,
        status_code: int = 200,
        status_text: str = "OK",
        content_charset: str = "utf-8",
        headers: list[tuple[str, str]] | None = None,
    ) -> None:
        self.content_type = content_type
        self.status_code = status_code
        self.status_text = status_text
        self.content_charset = content_charset
        self.headers = headers
        self._rendered_header = None

    def _render_header(self) -> bytes:
        if self._rendered_header is None:
            header = (
                f"HTTP/1.1 {self.status_code} {self.status_text}\r\n"
                + f"Content-Type: {self.content_type}; charset={self.content_charset}\r\n"
                + f"Content-Length: {self.content_length()}\r\n"
            )

            for key, value in (self.headers or dict()).items():
                header += f"{key}: {value}\r\n"

            header += "\r\n"

            self._rendered_header = header.encode("ISO-8859-1")

        return self._rendered_header

    def header_length(self) -> int:
        return len(self._rendered_header)

    def content_length(self) -> int:
        raise NotImplemented

    def length(self) -> int:
        return self.header_length() + self.content_length()

    def get_chunks(self) -> Iterator[bytes]:
        raise NotImplemented


class Response(BaseResponse):
    def __init__(
        self,
        content_type: str,
        data: bytes,
        status_code: int = 200,
        status_text: str = "OK",
        content_charset: str = "utf-8",
        headers: list[tuple[str, str]] | None = None,
    ) -> None:
        super().__init__(content_type, status_code, status_text, content_charset, headers)
        self.data = data

    def content_length(self) -> int:
        return len(self.data)

    def get_chunks(self) -> Iterator[bytes]:
        yield self._render_header()
        yield self.data


class StaticFileResponse(BaseResponse):
    def __init__(
        self,
        file_path: str,
        status_code: int = 200,
        status_text: str = "OK",
        content_charset: str = "utf-8",
        headers: list[tuple[str, str]] | None = None,
    ):
        self.file_path = file_path

        if file_path.endswith(".css"):
            mimetype = "text/css"
        elif file_path.endswith(".html"):
            mimetype = "text/html"
        elif file_path.endswith(".js"):
            mimetype = "text/javascript"
        elif file_path.endswith(".txt") or file_path.endswith("LICENSE"):
            mimetype = "text/plain"
        elif file_path.endswith(".woff"):
            mimetype = "font/woff"
        elif file_path.endswith(".woff2"):
            mimetype = "font/woff2"
        else:
            mimetype = "application/octet-stream"

        super().__init__(mimetype, status_code, status_text, content_charset, headers)

    def content_length(self) -> int:
        with open(self.file_path) as handler:
            return handler.seek(0, 2)

    def get_chunks(self) -> Iterator[bytes]:
        yield self._render_header()

        with open(self.file_path) as handler:
            file_size = handler.seek(0, 2)
            handler.seek(0, 0)

            while handler.tell() < file_size:
                yield handler.read(1024)


class JsonResponse(Response):
    def __init__(
        self,
        data: Any,
        status_code: int = 200,
        status_text: str = "OK",
        content_charset: str = "utf-8",
        headers: list[tuple[str, str]] | None = None,
    ):
        super().__init__(
            "application/json", json.dumps(data).encode(), status_code, status_text, content_charset, headers
        )


class Response404(Response):
    def __init__(self):
        super().__init__("text/html", b"<h1>404 - Not Found</h1>", 404, "Not Found")


class Response500(Response):
    def __init__(self):
        super().__init__("text/html", b"<h1>500 - Internal Server Error</h1>", 500, "Internal Server Error")
