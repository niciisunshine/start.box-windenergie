import time

from machine import Pin

import constants  # noqa F401 - needs to be loaded at startup to intialize START_TICKS
from real_time import fetch_real_time, iso8601
from runtime_variables import RuntimeVariables
from webserver import WebServer


def interrupt_service_routine(_: Pin) -> None:
    RuntimeVariables.update_on_hall_pulse()


def set_interrupt_service_routine(enabled: bool) -> None:
    constants.HALL_SENSOR_PIN.irq(interrupt_service_routine, Pin.IRQ_FALLING if enabled else 0)


class DisabledInterruptServiceRoutine:
    def __enter__(self) -> None:
        set_interrupt_service_routine(False)

    def __exit__(self, *args) -> None:
        set_interrupt_service_routine(True)


def main() -> None:
    RuntimeVariables.init()

    RuntimeVariables.get_wlan().connect()
    if not RuntimeVariables.get_wlan().is_access_point():
        fetch_real_time()
        print(f"Current time: {iso8601()}")

    with RuntimeVariables.get_webserver() as webserver:
        set_interrupt_service_routine(True)

        while True:
            loop(webserver)


def loop(webserver: WebServer) -> None:
    loop_start = time.ticks_ms()

    if RuntimeVariables.get_wlan().reconnect():
        try:
            webserver.non_blocking_accept_and_respond()
        except OSError as exc:
            print("Error while responding to or accepting a request:", repr(exc))

    with DisabledInterruptServiceRoutine():
        if RuntimeVariables.is_ready_to_calculate_and_reset():
            rpm = RuntimeVariables.get_current_rpm() // 10 - 2
            if rpm < 0:  # Range 0 - 29
                rpm = 0
            elif rpm >= 12:  # Range 140+
                rpm = 12

            value = constants.RPM_STORAGE.get(rpm)
            constants.RPM_STORAGE.set(rpm, value + 1)
            constants.RPM_STORAGE.commit()

        RuntimeVariables.check_current_rpm()

    RuntimeVariables.update_voltage()

    time_to_sleep = 500 - time.ticks_diff(time.ticks_ms(), loop_start)

    if time_to_sleep > 0:
        time.sleep_ms(time_to_sleep)


if __name__ == "__main__":
    main()
