SSIDS: list[tuple[bytes, str]] = ...
"""
A list of tuples containing the SSID in bytes as first tuple element and the key/password as second tuple element.
"""

RESET_PASSWORD: str = ...
"""
The password to supply in order to successfully reset the values.
"""

RANDOM_PASSWORD: str = ...
"""
The password to supply in order to successfully set random values.
"""

WLAN_AP_PASSWORD: str = ...
"""
The password to use when hosting the WLAN access point (must be at least 8 characters).
"""
