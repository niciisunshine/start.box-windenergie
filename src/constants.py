import time

from machine import Pin

from rpm_storage import RPMStorage

RPM_STORAGE = RPMStorage()

START_TICKS = time.ticks_ms()

HALL_SENSOR_PIN = Pin(15, Pin.IN, Pin.PULL_UP)
GENERATOR_VOLTAGE_PIN = Pin(32, Pin.IN)
OPERATING_VOLTAGE_PIN = Pin(33, Pin.IN)
