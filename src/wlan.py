import secrets
import time

import network


class _WlanBase:
    def is_access_point(self) -> bool:
        raise NotImplemented

    def connect(self) -> None:
        raise NotImplemented

    def reconnect(self) -> bool:
        raise NotImplemented


class WlanSta(_WlanBase):
    def __init__(self):
        self._wlan = network.WLAN(network.STA_IF)
        self._wlan.active(False)
        self._wlan.config(reconnects=5)
        self._wlan.active(True)

        self._tried_ssids = set()
        self._connecting = False

    def is_access_point(self) -> bool:
        return False

    def _non_blocking_connect(self, establish_connection_msg: str, connection_established_msg: str) -> bool:
        if self._wlan.status() != network.STAT_CONNECTING and self._wlan.status() != network.STAT_GOT_IP:
            available_ssids = set(ssid for (ssid, *_) in self._wlan.scan())
            not_tired_ssids = available_ssids - self._tried_ssids
            if not not_tired_ssids:
                not_tired_ssids = available_ssids
                self._tried_ssids = set()

            known_ssids = [(ssid, key) for (ssid, key) in secrets.SSIDS if ssid in not_tired_ssids]
            if known_ssids:
                ssid, key = known_ssids[0]
                self._wlan.connect(ssid, key)
                self._tried_ssids.add(ssid)

                self._connecting = True
                print(establish_connection_msg % {"ssid": ssid.decode()})

        if self._connecting and self._wlan.isconnected():
            self._connecting = False
            print(connection_established_msg % {"ip_address": self._wlan.ifconfig()[0]})

        return self._wlan.isconnected()

    def connect(self) -> None:
        while not self._non_blocking_connect(
            "Connecting to network %(ssid)s...", "Connected to network with the IP %(ip_address)s"
        ):
            time.sleep_ms(500)

    def reconnect(self) -> bool:
        return self._non_blocking_connect(
            "Reconnecting to network %(ssid)s...", "Reconnected to network with the IP %(ip_address)s"
        )


class WlanAP(_WlanBase):
    _ssid = "start.box - Windrad"
    _router_ip = "192.168.42.1"

    def __init__(self):
        self._wlan = network.WLAN(network.AP_IF)
        self._wlan.active(False)
        self._wlan.config(essid=self._ssid, password=secrets.WLAN_AP_PASSWORD, authmode=network.AUTH_WPA_WPA2_PSK)
        self._wlan.ifconfig((self._router_ip, "255.255.255.0", self._router_ip, self._router_ip))
        self._wlan.active(True)

    def is_access_point(self) -> bool:
        return True

    def connect(self) -> None:
        print(f"Running in access point mode with SSID '{self._ssid}' and IP {self._router_ip}")

    def reconnect(self) -> bool:
        return True
