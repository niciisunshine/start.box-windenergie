import esp32


class RPMStorage:
    def __init__(self):
        self._nvs = esp32.NVS("rpm")

    @classmethod
    def _check_range(cls, i: int) -> None:
        if 0 > i or i > 12:
            raise ValueError(f"The supplied rpm range {i} is out of range. It has to be between 0 and 12 (including).")

    def get(self, i: int) -> int:
        self._check_range(i)

        try:
            return self._nvs.get_i32(f"rpm-{i}")
        except OSError:
            return 0

    def set(self, i: int, value: int) -> None:
        self._check_range(i)

        self._nvs.set_i32(f"rpm-{i}", value)

    def commit(self):
        self._nvs.commit()
