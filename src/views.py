import json
import os
import random
import time
from http import BaseResponse, JsonResponse, Request, Response404, StaticFileResponse


def index_html(request: Request) -> BaseResponse:
    if request.method != "GET":
        return Response404()

    return StaticFileResponse("staticfiles/index.html")


def static_file(request: Request) -> BaseResponse:
    if request.method != "GET":
        return Response404()

    path = "staticfiles/" + request.location
    if os.stat(path)[0] != 32768:  # 32768 represents regular files
        return Response404()

    return StaticFileResponse(path)


def get_values(request: Request) -> BaseResponse:
    if request.method != "GET":
        return Response404()

    from constants import RPM_STORAGE, START_TICKS
    from runtime_variables import RuntimeVariables

    uptime = time.ticks_diff(time.ticks_ms(), START_TICKS) // 1_000

    data = {
        "currentGeneratorVoltage": RuntimeVariables.get_generator_voltage(),
        "currentOperatingVoltage": RuntimeVariables.get_operating_voltage(),
        "currentRPM": RuntimeVariables.get_current_rpm(),
        "rotations": [RPM_STORAGE.get(i) for i in range(13)],
        "uptime": uptime,
    }

    return JsonResponse(data)


def reset_values(request: Request) -> BaseResponse:
    import secrets

    from constants import RPM_STORAGE

    if request.method != "POST":
        return Response404()

    try:
        body = json.loads(request.body)

        if not isinstance(body, dict) or "password" not in body or not isinstance(body["password"], str):
            return JsonResponse({"type": "error", "reason": "Malformed request body."}, 400, "Bad Request")

        password = body["password"]
    except ValueError:
        return JsonResponse({"type": "error", "reason": "Unable to parse request."}, 400, "Bad Request")

    if password != secrets.RESET_PASSWORD:
        return JsonResponse({"type": "error", "reason": "Invalid password."}, 403, "Forbidden")

    for i in range(13):
        RPM_STORAGE.set(i, 0)
    RPM_STORAGE.commit()
    return JsonResponse({"type": "success"})


# TODO remove random
def random_values(request: Request) -> BaseResponse:
    import secrets

    from constants import RPM_STORAGE

    if request.method != "POST":
        return Response404()

    try:
        body = json.loads(request.body)

        if not isinstance(body, dict) or "password" not in body or not isinstance(body["password"], str):
            return JsonResponse({"type": "error", "reason": "Malformed request body."}, 400, "Bad Request")

        password = body["password"]
    except ValueError:
        return JsonResponse({"type": "error", "reason": "Unable to parse request."}, 400, "Bad Request")

    if password != secrets.RANDOM_PASSWORD:
        return JsonResponse({"type": "error", "reason": "Invalid password."}, 403, "Forbidden")

    for i in range(13):
        RPM_STORAGE.set(i, random.randint(0, 1337))
    RPM_STORAGE.commit()
    return JsonResponse({"type": "success"})
